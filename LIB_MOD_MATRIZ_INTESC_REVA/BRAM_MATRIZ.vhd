----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BRAM_MATRIZ is

generic(
			BRAM_TFT_MAX_ADD  	: INTEGER 	:=  8;			--FILAS DE LA MEMORIA.
			BRAM_TFT_BITS_ADD 	: INTEGER 	:=  8				--COLUMNAS DE LA MEMORIA.
			);

port(	LOCKED       	: in   std_logic;	--Bloqueo
		BRAM_TFT_CLKA  : in   std_logic;	--Frecuencia de relog del puerto a.
		BRAM_TFT_CLKB  : in   std_logic;	--Frecuencia de relog del puerto b.
		BRAM_TFT_ENA   : in   std_logic;	--Habilita a	
		BRAM_TFT_ENB   : in   std_logic;	--Habilita b
		BRAM_TFT_WRITE : in   std_logic;	--Escibir en a
		BRAM_TFT_READ  : in   std_logic;	--Leer en b
		BRAM_TFT_DIA   : in   std_logic_vector(BRAM_TFT_BITS_ADD-1 downto 0);	--Entrada a
		BRAM_TFT_DOB   : out  std_logic_vector(BRAM_TFT_BITS_ADD-1 downto 0);	--Salida b 
		BRAM_TFT_ADDRA : in   integer range 0 to BRAM_TFT_MAX_ADD-1;	--Direcci�n a
		BRAM_TFT_ADDRB : in   integer range 0 to BRAM_TFT_MAX_ADD-1		--Direcci�n b
);

end BRAM_MATRIZ;

architecture Behavioral of BRAM_MATRIZ is

type ram_type is array (0 TO BRAM_TFT_MAX_ADD-1) of std_logic_vector(BRAM_TFT_BITS_ADD-1 downto 0); 

shared variable RAM : ram_type := (OTHERS => (OTHERS => '0'));

begin

process(BRAM_TFT_CLKA,LOCKED)
begin
if LOCKED = '1' then
	if BRAM_TFT_CLKA'event and BRAM_TFT_CLKA = '1' then
		if BRAM_TFT_ENA = '1' then
			if BRAM_TFT_WRITE = '1' then 
			   RAM(BRAM_TFT_ADDRA) := BRAM_TFT_DIA;
			end if;
		end if;
	end if;
end if;
end process;

process(BRAM_TFT_CLKB,LOCKED)
begin
if LOCKED = '1' then
	if BRAM_TFT_CLKB'event and BRAM_TFT_CLKB = '1' then
		if BRAM_TFT_ENB = '1' then
			if BRAM_TFT_READ = '1' then
				BRAM_TFT_DOB <= RAM(BRAM_TFT_ADDRB);
			end if;
		end if;
	end if;
end if;
end process;


end Behavioral;

