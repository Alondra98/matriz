----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;


entity LIBRERIA_MOD_MATRIZ_INTESC is

generic(
			FPGA_CLK 			: INTEGER 	:= 50_000_000; --FRECUENCIA DEL FPGA.
			MAX_FILAS      	: INTEGER 	:=  8;			--FILAS DE LA MEMORIA.
			MAX_COLUMNAS		: INTEGER 	:=  8;			--COLUMNAS DE LA MEMORIA.
			SEL_ANODO_CATODO	: STD_LOGIC := '0'		   --SELECTOR DE �NODO ('0') O C�TODO ('1').
);

port ( CLK 	  	: in  std_logic;							 									--Relog del FPGA.
		 DATA_IN : in  std_logic_vector((MAX_FILAS+MAX_COLUMNAS)-1 downto 0);	--Dato de entrada
		 FILA 	: out std_logic_vector(MAX_FILAS-1 downto 0);	 					--Control de las filas de la matriz.
		 VECTOR  : out std_logic_vector(MAX_COLUMNAS-1 downto 0)   					--Control de las columnas de la matriz.
);

end LIBRERIA_MOD_MATRIZ_INTESC;

architecture Behavioral of LIBRERIA_MOD_MATRIZ_INTESC is

component BRAM_MATRIZ is

generic(
			BRAM_TFT_MAX_ADD  	: INTEGER 	:=  8;	--FILAS DE LA MEMORIA.
			BRAM_TFT_BITS_ADD 	: INTEGER 	:=  8		--COLUMNAS DE LA MEMORIA.
			);

port(	LOCKED       	: in   std_logic;	--Bloqueo
		BRAM_TFT_CLKA  : in   std_logic;	--Frecuencia de relog del puerto a.
		BRAM_TFT_CLKB  : in   std_logic;	--Frecuencia de relog del puerto b.
		BRAM_TFT_ENA   : in   std_logic;	--Habilita a	
		BRAM_TFT_ENB   : in   std_logic;	--Habilita b
		BRAM_TFT_WRITE : in   std_logic;	--Escibir en a
		BRAM_TFT_READ  : in   std_logic;	--Leer en b
		BRAM_TFT_DIA   : in   std_logic_vector(BRAM_TFT_BITS_ADD-1 downto 0);	--Entrada a
		BRAM_TFT_DOB   : out  std_logic_vector(BRAM_TFT_BITS_ADD-1 downto 0);	--Salida b 
		BRAM_TFT_ADDRA : in   integer range 0 to BRAM_TFT_MAX_ADD-1;	--Direcci�n a
		BRAM_TFT_ADDRB : in   integer range 0 to BRAM_TFT_MAX_ADD-1		--Direcci�n b
);

end component BRAM_MATRIZ;

CONSTANT DELAY			  : integer := 49_999;	--Constante para generar un retardo de 1ms
signal conta_delay	  : integer range 0 to DELAY := 0;				--Contador para el retardo de 1ms.
signal conta_fila		  : integer range 0 to MAX_FILAS-1 := 0;	--Contador para seleccionar las filas.
signal direccion       : integer range 0 to MAX_FILAS-1 := 0;	--Direcci�n de la memoria.

signal anodo_catodo    : std_logic:= '0';		--Se�al que toma el valor de SEL_ANODO_CATODO.
signal vector_ram		  : std_logic_vector(MAX_COLUMNAS-1 downto 0):= (others => '0'); --Se�al que obtiene las filas.

signal locked          : std_logic := '0';	--Bloqueo
signal bram_tft_clka   : std_logic := '0';	--Frecuencia de relog del puerto a.
signal bram_tft_clkb   : std_logic := '0';	--Frecuencia de relog del puerto b.
signal bram_tft_ena    : std_logic := '0';	--Habilita a	
signal bram_tft_enb    : std_logic := '0';	--Habilita b
signal bram_tft_write  : std_logic := '0';	--Escibir en a
signal bram_tft_read   : std_logic := '0';	--Leer en b
signal bram_tft_dia    : std_logic_vector(MAX_COLUMNAS-1 downto 0):= (others => '0');	--Entrada a
signal bram_tft_dob    : std_logic_vector(MAX_COLUMNAS-1 downto 0):= (others => '0');	--Salida b
signal bram_tft_addra  : integer range 0 to MAX_FILAS-1;	  		--Direcci�n a
signal bram_tft_addrb  : integer range 0 to MAX_FILAS-1:= 0;	--Direcci�n b 	

type ram_type is array (0 TO MAX_FILAS-1) of std_logic_vector(MAX_COLUMNAS-1 downto 0); 

shared variable RAM : ram_type := (OTHERS => (OTHERS => '0'));

begin

--Instancia para conectar la "BRAM"

U1:	BRAM_MATRIZ

		generic map( BRAM_TFT_MAX_ADD 	=> MAX_FILAS ,		--FILAS DE LA MEMORIA.
						 BRAM_TFT_BITS_ADD 	=> MAX_COLUMNAS)	--COLUMNAS DE LA MEMORIA.
						
		port map (locked,bram_tft_clka,bram_tft_clkb,bram_tft_ena,bram_tft_enb,bram_tft_write,
		          bram_tft_read,bram_tft_dia,bram_tft_dob,bram_tft_addra,bram_tft_addrb);

--Proceso para sacar los vectores de la BRAM_B

process(CLK)
begin
	if rising_edge(CLK) then
		conta_delay <= conta_delay +1;
		vector_ram  <= bram_tft_dob;
		if	anodo_catodo = '1' then
			VECTOR <= vector_ram;
		elsif anodo_catodo = '0' then
			VECTOR <= not vector_ram;
		end if;
		if conta_delay = DELAY then
			conta_delay <= 0;
			conta_fila  <= conta_fila+1;
			bram_tft_addrb <= bram_tft_addrb+1;
			if conta_fila = MAX_FILAS then
				conta_fila <= 0;
				direccion <= bram_tft_addrb;
				if	anodo_catodo = '1' then
					FILA <= conv_std_logic_vector (direccion,8);
				elsif anodo_catodo = '0' then
					FILA <= not conv_std_logic_vector (direccion,8);
				end if;
			end if;
		end if;
	end if;
end process;

bram_tft_addra	<= conv_integer(DATA_IN((MAX_FILAS+MAX_COLUMNAS)-1 downto (MAX_FILAS+MAX_COLUMNAS)/2));
bram_tft_dia	<= DATA_IN(((MAX_FILAS+MAX_COLUMNAS)/2)-1 downto 0);

anodo_catodo 	<= SEL_ANODO_CATODO;	--Asignamos a la se�al anodo_catodo '0' o '1' dependiendo de SEL_ANODO_CATODO.

 
end Behavioral;

